def is_triangle(a, b, c):
    if a <= 0 or b <= 0 or c <= 0:
        # All sides must have positive lengths
        return False
    
    # Check the triangle inequality theorem
    if a + b > c and a + c > b and b + c > a:
        return True
    else:
        return False
